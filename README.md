# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
|Guilherme|@GuilhermeRds19|
|Gladson|-|
|Fernando|-|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://cursoseaulas.gitlab.io/mypage 

# Links Úteis

* [Tutorial HTML](http://pt-br.html.net/tutorials/html/)
* [Gnuplot](http://fiscomp.if.ufrgs.br/index.php/Gnuplot)
